## [1.0.5](https://github.com/JoshPiper/gm_sysinfo/compare/v1.0.4...v1.0.5) (2021-12-31)


### Bug Fixes

* **build:** Checkout on the tagged version, not the pre-tag version. ([0435de0](https://github.com/JoshPiper/gm_sysinfo/commit/0435de0cc71b9f59a2dca09653ab3c51feaf4100))



## [1.0.4](https://github.com/JoshPiper/gm_sysinfo/compare/v1.0.3...v1.0.4) (2021-12-31)


### Bug Fixes

* **build:** Automatically bump the cargo lock file. ([d7de695](https://github.com/JoshPiper/gm_sysinfo/commit/d7de695f6fc68db643d398db9dc10d59f0e76068))



## [1.0.3](https://github.com/JoshPiper/gm_sysinfo/compare/v1.0.2...v1.0.3) (2021-12-31)


### Bug Fixes

* **deps:** Update minimum version of gmod-rs, adds fix for client console crashing. ([083bfdb](https://github.com/JoshPiper/gm_sysinfo/commit/083bfdbafc1e36bc023325d4d510c8264da9a172))



## [1.0.2](https://github.com/JoshPiper/gm_sysinfo/compare/v1.0.1...v1.0.2) (2021-12-31)


### Bug Fixes

* Allow any stringable to be passed to error. ([4f7921f](https://github.com/JoshPiper/gm_sysinfo/commit/4f7921fecf7fee7b617352cbd8256acc196539da))



## [1.0.1](https://github.com/JoshPiper/gm_sysinfo/compare/v1.0.0...v1.0.1) (2021-12-31)


### Bug Fixes

* **build:** Checkout all commits, to make sure the changelogs properly build. ([7bd762a](https://github.com/JoshPiper/gm_sysinfo/commit/7bd762a82949d3531dd9ca6d0770f6b75180de94))
* call lua.error directly. ([4c37422](https://github.com/JoshPiper/gm_sysinfo/commit/4c37422692f07484e5a0dbd2e0c94a349ebd0556))
* Rewrote internal clone/borrow rules. ([406b1c6](https://github.com/JoshPiper/gm_sysinfo/commit/406b1c6edb1567088d69c0ac4c29d35211aa090b))


### Performance Improvements

* Only deref our lazy static once. ([5977349](https://github.com/JoshPiper/gm_sysinfo/commit/5977349205b69243914ffa230d212eed9173fba3))



